[Electron.NET](https://github.com/ElectronNET/Electron.NET)

### Requirements:
* [.NET 5 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
* [Node.js 16.14.2 (LTS) + npm 8.5.0](https://nodejs.org/en/download/)
* [ElectronNET.CLI](https://www.nuget.org/packages/ElectronNET.CLI/)

### Start application:
`electronize start`
